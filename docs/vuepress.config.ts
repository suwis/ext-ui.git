import { defineUserConfig, defaultTheme } from 'vuepress'
// import { docsearchPlugin } from "@vuepress/plugin-docsearch";
import { searchPlugin } from '@vuepress/plugin-search'

export default defineUserConfig({
  lang: 'zh-CN',
  title: 'ext-ui',
  description: 'ext-ui',
  base: '/ext-ui/',
  head: [['link', { rel: 'icon', href: '/ext-ui/logo.svg' }]],
  plugins: [
    searchPlugin({
      // 配置项
    })
  ],
  theme: defaultTheme({
    navbar: [
      {
        text: '开发指南',
        link: '/guide'
      },
      {
        text: '组件',
        link: '/components'
      },
      {
        text: '更新日志',
        link: '/change-log'
      }
    ]
  }),
  markdown: {
    code: {
      lineNumbers: false
    }
  }
})

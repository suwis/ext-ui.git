# 快速上手

## 安装

通过 npm 安装

```sh
npm i @suwis/ext-ui
```

通过 yarn 安装

```sh
yarn add @suwis/ext-ui
```

通过 pnpm 安装

```sh
pnpm add @suwis/ext-ui
```

## uni-app 自动引入

pages.json 加入以下配置

```json
{
  "easycom": {
    "autoscan": true,
    "custom": {
      "ext-(.*)": "@suwis/ext-ui/lib/uni-app/ext-$1/index.vue"
    }
  }
}
```

在页面中使用

```vue
<template>
  <view>
    <ext-editor />
  </view>
</template>
```

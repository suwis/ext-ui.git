import{_ as a,o as n,c as s,d as t}from"./app-b7ee7e85.js";const p={},e=t(`<h1 id="快速上手" tabindex="-1"><a class="header-anchor" href="#快速上手" aria-hidden="true">#</a> 快速上手</h1><h2 id="安装" tabindex="-1"><a class="header-anchor" href="#安装" aria-hidden="true">#</a> 安装</h2><p>通过 npm 安装</p><div class="language-bash" data-ext="sh"><pre class="language-bash"><code><span class="token function">npm</span> i @suwis/ext-ui
</code></pre></div><p>通过 yarn 安装</p><div class="language-bash" data-ext="sh"><pre class="language-bash"><code><span class="token function">yarn</span> <span class="token function">add</span> @suwis/ext-ui
</code></pre></div><p>通过 pnpm 安装</p><div class="language-bash" data-ext="sh"><pre class="language-bash"><code><span class="token function">pnpm</span> <span class="token function">add</span> @suwis/ext-ui
</code></pre></div><h2 id="uni-app-自动引入" tabindex="-1"><a class="header-anchor" href="#uni-app-自动引入" aria-hidden="true">#</a> uni-app 自动引入</h2><p>pages.json 加入以下配置</p><div class="language-json" data-ext="json"><pre class="language-json"><code><span class="token punctuation">{</span>
  <span class="token property">&quot;easycom&quot;</span><span class="token operator">:</span> <span class="token punctuation">{</span>
    <span class="token property">&quot;autoscan&quot;</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token property">&quot;custom&quot;</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      <span class="token property">&quot;ext-(.*)&quot;</span><span class="token operator">:</span> <span class="token string">&quot;@suwis/ext-ui/lib/uni-app/ext-$1/index.vue&quot;</span>
    <span class="token punctuation">}</span>
  <span class="token punctuation">}</span>
<span class="token punctuation">}</span>
</code></pre></div><p>在页面中使用</p><div class="language-vue" data-ext="vue"><pre class="language-vue"><code><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>template</span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>view</span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>ext-editor</span> <span class="token punctuation">/&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>view</span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>template</span><span class="token punctuation">&gt;</span></span>
</code></pre></div>`,13),o=[e];function c(u,l){return n(),s("div",null,o)}const r=a(p,[["render",c],["__file","guide.html.vue"]]);export{r as default};

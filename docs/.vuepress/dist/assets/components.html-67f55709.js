import{_ as t,o as n,c as a,d as s}from"./app-b7ee7e85.js";const p={},o=s(`<h2 id="ext-editor-富文本编辑器" tabindex="-1"><a class="header-anchor" href="#ext-editor-富文本编辑器" aria-hidden="true">#</a> ext-editor 富文本编辑器</h2><blockquote><p>基础用法</p></blockquote><div class="language-html" data-ext="html"><pre class="language-html"><code><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>template</span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>ext-editor</span> <span class="token attr-name">ref</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">&quot;</span>editor<span class="token punctuation">&quot;</span></span> <span class="token attr-name">:upload-host</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">&quot;</span>uploadurl<span class="token punctuation">&quot;</span></span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>ext-editor</span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>template</span><span class="token punctuation">&gt;</span></span>
</code></pre></div><p>props</p><table><thead><tr><th>属性</th><th>说明</th><th>类型</th><th>默认值</th></tr></thead><tbody><tr><td>icon-size</td><td>功能图标大小</td><td>String</td><td>&#39;39rpx&#39;</td></tr><tr><td>btn-move-color</td><td>移动板块按钮颜色</td><td>String</td><td>&#39;&#39;</td></tr><tr><td>btn-delete-color</td><td>删除板块按钮颜色</td><td>String</td><td>&#39;&#39;</td></tr><tr><td>btn-color</td><td>功能按钮颜色</td><td>String</td><td>&#39;&#39;</td></tr><tr><td>text-color</td><td>段落文字颜色</td><td>String</td><td>&#39;&#39;</td></tr><tr><td>adapter</td><td>组件适配器</td><td>Object</td><td>{}</td></tr><tr><td>upload-host</td><td>上传文件接口地址</td><td>String</td><td>-</td></tr><tr><td>upload-header</td><td>上传文件头</td><td>Object</td><td>{}</td></tr><tr><td>upload-form-data</td><td>上传携带表单</td><td>Object</td><td>{}</td></tr><tr><td>upload-file-name</td><td>上传文件名</td><td>String</td><td>file</td></tr></tbody></table><p>adapter 对象说明</p><blockquote><p>上传方法适配</p></blockquote><div class="language-javascript" data-ext="js"><pre class="language-javascript"><code>adapter<span class="token punctuation">.</span><span class="token function-variable function">upload</span> <span class="token operator">=</span> <span class="token punctuation">(</span><span class="token parameter">result</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  <span class="token keyword">let</span> data <span class="token operator">=</span> <span class="token constant">JSON</span><span class="token punctuation">.</span><span class="token function">parse</span><span class="token punctuation">(</span>result<span class="token punctuation">.</span>data<span class="token punctuation">)</span>
  <span class="token keyword">return</span> <span class="token punctuation">{</span>
    <span class="token literal-property property">url</span><span class="token operator">:</span> data<span class="token punctuation">.</span>url <span class="token operator">||</span> <span class="token string">&#39;&#39;</span>
  <span class="token punctuation">}</span>
<span class="token punctuation">}</span>
</code></pre></div><p>slot</p><table><thead><tr><th>方法名</th><th>说明</th></tr></thead><tbody><tr><td>poster-option</td><td>大图底部插槽</td></tr></tbody></table><p>event</p><table><thead><tr><th>方法名</th><th>说明</th><th>参数返回值</th></tr></thead><tbody><tr><td>option-tap</td><td>大图点击</td><td>Object</td></tr></tbody></table><p>methods</p><table><thead><tr><th>方法名</th><th>说明</th><th>参数</th><th>返回值</th></tr></thead><tbody><tr><td>parseHtml</td><td>返回富文本内容</td><td>-</td><td>html</td></tr><tr><td>setData</td><td>设置富文本内容</td><td>Array</td><td>-</td></tr><tr><td>getData</td><td>获取富文本 JSON 数组</td><td>-</td><td>Array</td></tr></tbody></table><p>setData 参数格式</p><div class="language-javascript" data-ext="js"><pre class="language-javascript"><code><span class="token keyword">this</span><span class="token punctuation">.</span>$refs<span class="token punctuation">.</span>editor<span class="token punctuation">.</span><span class="token function">setData</span><span class="token punctuation">(</span><span class="token punctuation">[</span>
  <span class="token comment">// 大图</span>
  <span class="token punctuation">{</span>
    <span class="token literal-property property">type</span><span class="token operator">:</span> <span class="token string">&#39;poster&#39;</span><span class="token punctuation">,</span>
    <span class="token literal-property property">src</span><span class="token operator">:</span> <span class="token string">&#39;https://xnit.funtask.club/b5bd72a5-14e5-470f-b5ca-5f78d59c9f0d.png&#39;</span>
  <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token comment">// 九宫格图</span>
  <span class="token punctuation">{</span>
    <span class="token literal-property property">type</span><span class="token operator">:</span> <span class="token string">&#39;images&#39;</span><span class="token punctuation">,</span>
    <span class="token literal-property property">list</span><span class="token operator">:</span> <span class="token punctuation">[</span>
      <span class="token string">&#39;https://xnit.funtask.club/b5bd72a5-14e5-470f-b5ca-5f78d59c9f0d.png&#39;</span><span class="token punctuation">,</span>
      <span class="token string">&#39;https://xnit.funtask.club/b5bd72a5-14e5-470f-b5ca-5f78d59c9f0d.png&#39;</span><span class="token punctuation">,</span>
      <span class="token string">&#39;https://xnit.funtask.club/b5bd72a5-14e5-470f-b5ca-5f78d59c9f0d.png&#39;</span>
    <span class="token punctuation">]</span>
  <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token comment">// 视频</span>
  <span class="token punctuation">{</span>
    <span class="token literal-property property">type</span><span class="token operator">:</span> <span class="token string">&#39;video&#39;</span><span class="token punctuation">,</span>
    <span class="token literal-property property">src</span><span class="token operator">:</span> <span class="token string">&#39;https://xnit.funtask.club/b5bd72a5-14e5-470f-b5ca-5f78d59c9f0d.mp4&#39;</span>
  <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token comment">// 文本</span>
  <span class="token punctuation">{</span>
    <span class="token literal-property property">type</span><span class="token operator">:</span> <span class="token string">&#39;text&#39;</span><span class="token punctuation">,</span>
    <span class="token literal-property property">content</span><span class="token operator">:</span> <span class="token string">&#39;hello world&#39;</span>
  <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token comment">// 标签</span>
  <span class="token punctuation">{</span>
    <span class="token literal-property property">type</span><span class="token operator">:</span> <span class="token string">&#39;tags&#39;</span><span class="token punctuation">,</span>
    <span class="token literal-property property">list</span><span class="token operator">:</span> <span class="token punctuation">[</span><span class="token string">&#39;新品上市&#39;</span><span class="token punctuation">,</span> <span class="token string">&#39;活动促销&#39;</span><span class="token punctuation">]</span>
  <span class="token punctuation">}</span>
<span class="token punctuation">]</span><span class="token punctuation">)</span>
</code></pre></div>`,16),e=[o];function c(l,r){return n(),a("div",null,e)}const u=t(p,[["render",c],["__file","components.html.vue"]]);export{u as default};

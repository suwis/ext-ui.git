## ext-editor 富文本编辑器

> 基础用法

```html
<template>
  <ext-editor ref="editor" :upload-host="uploadurl"></ext-editor>
</template>
```

props

| 属性             | 说明             | 类型   | 默认值  |
| ---------------- | ---------------- | ------ | ------- |
| icon-size        | 功能图标大小     | String | '39rpx' |
| btn-move-color   | 移动板块按钮颜色 | String | ''      |
| btn-delete-color | 删除板块按钮颜色 | String | ''      |
| btn-color        | 功能按钮颜色     | String | ''      |
| text-color       | 段落文字颜色     | String | ''      |
| adapter          | 组件适配器       | Object | {}      |
| upload-host      | 上传文件接口地址 | String | -       |
| upload-header    | 上传文件头       | Object | {}      |
| upload-form-data | 上传携带表单     | Object | {}      |
| upload-file-name | 上传文件名       | String | file    |

adapter 对象说明

> 上传方法适配

```js
adapter.upload = (result) => {
  let data = JSON.parse(result.data)
  return {
    url: data.url || ''
  }
}
```

slot

| 方法名        | 说明         |
| ------------- | ------------ |
| poster-option | 大图底部插槽 |

event

| 方法名     | 说明     | 参数返回值 |
| ---------- | -------- | ---------- |
| option-tap | 大图点击 | Object     |

methods

| 方法名    | 说明                 | 参数  | 返回值 |
| --------- | -------------------- | ----- | ------ |
| parseHtml | 返回富文本内容       | -     | html   |
| setData   | 设置富文本内容       | Array | -      |
| getData   | 获取富文本 JSON 数组 | -     | Array  |

setData 参数格式

```js
this.$refs.editor.setData([
  // 大图
  {
    type: 'poster',
    src: 'https://xnit.funtask.club/b5bd72a5-14e5-470f-b5ca-5f78d59c9f0d.png'
  },
  // 九宫格图
  {
    type: 'images',
    list: [
      'https://xnit.funtask.club/b5bd72a5-14e5-470f-b5ca-5f78d59c9f0d.png',
      'https://xnit.funtask.club/b5bd72a5-14e5-470f-b5ca-5f78d59c9f0d.png',
      'https://xnit.funtask.club/b5bd72a5-14e5-470f-b5ca-5f78d59c9f0d.png'
    ]
  },
  // 视频
  {
    type: 'video',
    src: 'https://xnit.funtask.club/b5bd72a5-14e5-470f-b5ca-5f78d59c9f0d.mp4'
  },
  // 文本
  {
    type: 'text',
    content: 'hello world'
  },
  // 标签
  {
    type: 'tags',
    list: ['新品上市', '活动促销']
  }
])
```
